/*
  Server setup
  Whiteline Builders.
*/

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

// setting up express app
const app = express();

// Including routes
var admin = require('./routes/adminRoute');
var api = require('./routes/comApiRoute');

// Body parser middleware
app.use(bodyParser.json({limit: '50mb', extended: true}));
app.use(bodyParser.urlencoded({extended: false}));

// CORS Middleware
app.use(cors());

// Setting up route paths
app.use('/api/admin', admin);
app.use('/api', api);

// Set static folder
app.use(express.static('public'));

// Index route
app.get('*', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});

var port_number = app.listen(process.env.PORT || 4000);
app.listen(port_number);
