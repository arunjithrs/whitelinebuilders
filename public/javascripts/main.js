
$(document).ready(function(){
	$(window).bind('scroll', function () {
	    if ($(window).scrollTop() > 250) {
	        $('#top-header').addClass('fixed');
	        $('body').addClass('scrolling');

	        if ($(window).scrollTop() > 350) {
	        	$('#top-header').addClass('sticky');
	        }
	    } else {
	        $('#top-header').removeClass('fixed');
	        $('#top-header').removeClass('sticky');
	    }
	    if ($(window).scrollTop() < 1) {
	        $('body').removeClass('scrolling');
	    }
	});
})